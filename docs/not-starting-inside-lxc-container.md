# MariaDB server fails to start inside LXC container

> **Solved by setting `lxc config set <container> security.nesting true` for the container.**
> See the `lxd-server` role.


On the server we are running LXC v3.0.3:
```
taha@luxor:~
$ lxc --version
3.0.3
```

Very relevant reports:

+ https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=920643
+ https://github.com/lxc/lxc/pull/2758

The above reports suggest that we set
```
lxc.apparmor.profile = generated
lxc.apparmor.allow_nesting = 1
```
in `/etc/lxc/default.conf` (on the server).
Ok, added the above lines to the file on luxor. It now contains six lines:
```
lxc.net.0.type = veth
lxc.net.0.link = lxcbr0
lxc.net.0.flags = up
lxc.net.0.hwaddr = 00:16:3e:xx:xx:xx
lxc.apparmor.profile = generated
lxc.apparmor.allow_nesting = 1
```

Then I restarted both the `lxd.service` and the `lxd-containers.service` (not sure this was necessary) and rebooted the container.
No effect, MariaDB service still fails to start.



The debian bug report also suggested a different fix,
namely removing the following three lines from the MariaDB service file:
```
ProtectSystem=full
PrivateDevices=true
ProtectHome=true
```
Since this change can be done on the container, I prefer it. Let's try it first.
```
taha@shanghai:~
$ sudo nano /lib/systemd/system/mariadb.service
$ sudo systemctl daemon-reload
$ sudo systemctl restart mariadb.service
```
No, this did not resolve anything. I rebooted the container, nothing changed, same error as before.

Let's try this again, but instead of commenting out, let's flip their values.
Nope, same error.


[This issue](https://github.com/lxc/lxd/issues/7065) appears related, and suggests
setting `security.nesting=true` in the LXC container's settings.
Which settings exactly...? That would be:
`lxc config set <container> security.nesting true`

[Another issue](https://github.com/lxc/lxd/issues/7085), 
very relevant to my observations, also suggesting `security.nesting`, and with
a reply from Stephane Graber giving some background to this whole mess.
The messy situation with systemd and apparmor only underscores the danger of 
running privileged containers unless you are very knowledgeable about what you
are doing.

+ https://github.com/lxc/lxd/issues/7085
+ https://github.com/lxc/lxd/issues/8252


An older LXC ticket suggests setting `keyctl errno 38` in the LXC server's seccomp profile: `/usr/share/lxc/config/common.seccomp`.
https://github.com/lxc/lxc/issues/2449



## AppArmor settings?

```
taha@luxor:~
$ sudo aa-status 
apparmor module is loaded.
127 profiles are loaded.
123 profiles are in enforce mode.
   /sbin/dhclient
   /snap/core/11316/usr/lib/snapd/snap-confine
   /snap/core/11316/usr/lib/snapd/snap-confine//mount-namespace-capture-helper
   /usr/bin/lxc-start
   /usr/bin/man
   /usr/lib/NetworkManager/nm-dhcp-client.action
   /usr/lib/NetworkManager/nm-dhcp-helper
   /usr/lib/connman/scripts/dhclient-script
   /usr/lib/snapd/snap-confine
   /usr/lib/snapd/snap-confine//mount-namespace-capture-helper
   /usr/sbin/libvirtd
   /usr/sbin/libvirtd//qemu_bridge_helper
   /usr/sbin/ntpd
   /usr/sbin/tcpdump
   :lxd-bilbeis_<var-lib-lxd>:/snap/snapd/12159/usr/lib/snapd/snap-confine
   :lxd-bilbeis_<var-lib-lxd>:/snap/snapd/12159/usr/lib/snapd/snap-confine//mount-namespace-capture-helper
   :lxd-bilbeis_<var-lib-lxd>:/snap/snapd/12398/usr/lib/snapd/snap-confine
   :lxd-bilbeis_<var-lib-lxd>:/snap/snapd/12398/usr/lib/snapd/snap-confine//mount-namespace-capture-helper
   :lxd-bilbeis_<var-lib-lxd>:/usr/bin/man
   :lxd-bilbeis_<var-lib-lxd>:/usr/lib/NetworkManager/nm-dhcp-client.action
   :lxd-bilbeis_<var-lib-lxd>:/usr/lib/NetworkManager/nm-dhcp-helper
   :lxd-bilbeis_<var-lib-lxd>:/usr/lib/connman/scripts/dhclient-script
   :lxd-bilbeis_<var-lib-lxd>:/usr/lib/snapd/snap-confine
   :lxd-bilbeis_<var-lib-lxd>:/usr/lib/snapd/snap-confine//mount-namespace-capture-helper
   :lxd-bilbeis_<var-lib-lxd>:/usr/sbin/ntpd
   :lxd-bilbeis_<var-lib-lxd>:/usr/sbin/tcpdump
   :lxd-bilbeis_<var-lib-lxd>:/usr/sbin/unbound
   :lxd-bilbeis_<var-lib-lxd>:/{,usr/}sbin/dhclient
   :lxd-bilbeis_<var-lib-lxd>:lsb_release
   :lxd-bilbeis_<var-lib-lxd>:man_filter
   :lxd-bilbeis_<var-lib-lxd>:man_groff
   :lxd-bilbeis_<var-lib-lxd>:nvidia_modprobe
   :lxd-bilbeis_<var-lib-lxd>:nvidia_modprobe//kmod
   :lxd-bilbeis_<var-lib-lxd>:snap-update-ns.lxd
   :lxd-bilbeis_<var-lib-lxd>:snap.lxd.activate
   :lxd-bilbeis_<var-lib-lxd>:snap.lxd.benchmark
   :lxd-bilbeis_<var-lib-lxd>:snap.lxd.buginfo
   :lxd-bilbeis_<var-lib-lxd>:snap.lxd.check-kernel
   :lxd-bilbeis_<var-lib-lxd>:snap.lxd.daemon
   :lxd-bilbeis_<var-lib-lxd>:snap.lxd.hook.configure
   :lxd-bilbeis_<var-lib-lxd>:snap.lxd.hook.install
   :lxd-bilbeis_<var-lib-lxd>:snap.lxd.hook.remove
   :lxd-bilbeis_<var-lib-lxd>:snap.lxd.lxc
   :lxd-bilbeis_<var-lib-lxd>:snap.lxd.lxc-to-lxd
   :lxd-bilbeis_<var-lib-lxd>:snap.lxd.lxd
   :lxd-bilbeis_<var-lib-lxd>:snap.lxd.migrate
   :lxd-hunan_<var-lib-lxd>:/sbin/dhclient
   :lxd-hunan_<var-lib-lxd>:/usr/bin/lxc-start
   :lxd-hunan_<var-lib-lxd>:/usr/bin/man
   :lxd-hunan_<var-lib-lxd>:/usr/lib/NetworkManager/nm-dhcp-client.action
   :lxd-hunan_<var-lib-lxd>:/usr/lib/NetworkManager/nm-dhcp-helper
   :lxd-hunan_<var-lib-lxd>:/usr/lib/connman/scripts/dhclient-script
   :lxd-hunan_<var-lib-lxd>:/usr/lib/snapd/snap-confine
   :lxd-hunan_<var-lib-lxd>:/usr/lib/snapd/snap-confine//mount-namespace-capture-helper
   :lxd-hunan_<var-lib-lxd>:/usr/sbin/ntpd
   :lxd-hunan_<var-lib-lxd>:/usr/sbin/tcpdump
   :lxd-hunan_<var-lib-lxd>:lxc-container-default
   :lxd-hunan_<var-lib-lxd>:lxc-container-default-cgns
   :lxd-hunan_<var-lib-lxd>:lxc-container-default-with-mounting
   :lxd-hunan_<var-lib-lxd>:lxc-container-default-with-nesting
   :lxd-hunan_<var-lib-lxd>:man_filter
   :lxd-hunan_<var-lib-lxd>:man_groff
   :lxd-shanghai_<var-lib-lxd>:/snap/snapd/12159/usr/lib/snapd/snap-confine
   :lxd-shanghai_<var-lib-lxd>:/snap/snapd/12159/usr/lib/snapd/snap-confine//mount-namespace-capture-helper
   :lxd-shanghai_<var-lib-lxd>:/snap/snapd/12398/usr/lib/snapd/snap-confine
   :lxd-shanghai_<var-lib-lxd>:/snap/snapd/12398/usr/lib/snapd/snap-confine//mount-namespace-capture-helper
   :lxd-shanghai_<var-lib-lxd>:/usr/bin/man
   :lxd-shanghai_<var-lib-lxd>:/usr/lib/NetworkManager/nm-dhcp-client.action
   :lxd-shanghai_<var-lib-lxd>:/usr/lib/NetworkManager/nm-dhcp-helper
   :lxd-shanghai_<var-lib-lxd>:/usr/lib/connman/scripts/dhclient-script
   :lxd-shanghai_<var-lib-lxd>:/usr/lib/snapd/snap-confine
   :lxd-shanghai_<var-lib-lxd>:/usr/lib/snapd/snap-confine//mount-namespace-capture-helper
   :lxd-shanghai_<var-lib-lxd>:/usr/sbin/ntpd
   :lxd-shanghai_<var-lib-lxd>:/usr/sbin/tcpdump
   :lxd-shanghai_<var-lib-lxd>:/{,usr/}sbin/dhclient
   :lxd-shanghai_<var-lib-lxd>:lsb_release
   :lxd-shanghai_<var-lib-lxd>:man_filter
   :lxd-shanghai_<var-lib-lxd>:man_groff
   :lxd-shanghai_<var-lib-lxd>:nvidia_modprobe
   :lxd-shanghai_<var-lib-lxd>:nvidia_modprobe//kmod
   :lxd-shanghai_<var-lib-lxd>:snap-update-ns.lxd
   :lxd-shanghai_<var-lib-lxd>:snap.lxd.activate
   :lxd-shanghai_<var-lib-lxd>:snap.lxd.benchmark
   :lxd-shanghai_<var-lib-lxd>:snap.lxd.buginfo
   :lxd-shanghai_<var-lib-lxd>:snap.lxd.check-kernel
   :lxd-shanghai_<var-lib-lxd>:snap.lxd.daemon
   :lxd-shanghai_<var-lib-lxd>:snap.lxd.hook.configure
   :lxd-shanghai_<var-lib-lxd>:snap.lxd.hook.install
   :lxd-shanghai_<var-lib-lxd>:snap.lxd.hook.remove
   :lxd-shanghai_<var-lib-lxd>:snap.lxd.lxc
   :lxd-shanghai_<var-lib-lxd>:snap.lxd.lxc-to-lxd
   :lxd-shanghai_<var-lib-lxd>:snap.lxd.lxd
   :lxd-shanghai_<var-lib-lxd>:snap.lxd.migrate
   :lxd-xian_<var-lib-lxd>:/sbin/dhclient
   :lxd-xian_<var-lib-lxd>:/usr/bin/lxc-start
   :lxd-xian_<var-lib-lxd>:/usr/bin/man
   :lxd-xian_<var-lib-lxd>:/usr/lib/NetworkManager/nm-dhcp-client.action
   :lxd-xian_<var-lib-lxd>:/usr/lib/NetworkManager/nm-dhcp-helper
   :lxd-xian_<var-lib-lxd>:/usr/lib/connman/scripts/dhclient-script
   :lxd-xian_<var-lib-lxd>:/usr/lib/snapd/snap-confine
   :lxd-xian_<var-lib-lxd>:/usr/lib/snapd/snap-confine//mount-namespace-capture-helper
   :lxd-xian_<var-lib-lxd>:/usr/sbin/ntpd
   :lxd-xian_<var-lib-lxd>:/usr/sbin/tcpdump
   :lxd-xian_<var-lib-lxd>:lxc-container-default
   :lxd-xian_<var-lib-lxd>:lxc-container-default-cgns
   :lxd-xian_<var-lib-lxd>:lxc-container-default-with-mounting
   :lxd-xian_<var-lib-lxd>:lxc-container-default-with-nesting
   :lxd-xian_<var-lib-lxd>:man_filter
   :lxd-xian_<var-lib-lxd>:man_groff
   lxc-container-default
   lxc-container-default-cgns
   lxc-container-default-with-mounting
   lxc-container-default-with-nesting
   lxd-bilbeis_</var/lib/lxd>
   lxd-hunan_</var/lib/lxd>
   lxd-shanghai_</var/lib/lxd>
   lxd-xian_</var/lib/lxd>
   man_filter
   man_groff
   snap-update-ns.certbot
   snap-update-ns.core
   snap.core.hook.configure
   virt-aa-helper
4 profiles are in complain mode.
   snap.certbot.certbot
   snap.certbot.hook.configure
   snap.certbot.hook.prepare-plug-plugin
   snap.certbot.renew
108 processes have profiles defined.
108 processes are in enforce mode.
   /usr/sbin/libvirtd (2014) 
   lxd-bilbeis_</var/lib/lxd>//&:lxd-bilbeis_<var-lib-lxd>:/usr/sbin/ntpd (621) 
   lxd-bilbeis_</var/lib/lxd>//&:lxd-bilbeis_<var-lib-lxd>:/usr/sbin/unbound (6597) 
   lxd-bilbeis_</var/lib/lxd>//&:lxd-bilbeis_<var-lib-lxd>:unconfined (731) 
   lxd-bilbeis_</var/lib/lxd>//&:lxd-bilbeis_<var-lib-lxd>:unconfined (3819) 
   lxd-bilbeis_</var/lib/lxd>//&:lxd-bilbeis_<var-lib-lxd>:unconfined (3939) 
   lxd-bilbeis_</var/lib/lxd>//&:lxd-bilbeis_<var-lib-lxd>:unconfined (4090) 
   lxd-bilbeis_</var/lib/lxd>//&:lxd-bilbeis_<var-lib-lxd>:unconfined (4388) 
   lxd-bilbeis_</var/lib/lxd>//&:lxd-bilbeis_<var-lib-lxd>:unconfined (4392) 
   lxd-bilbeis_</var/lib/lxd>//&:lxd-bilbeis_<var-lib-lxd>:unconfined (4397) 
   lxd-bilbeis_</var/lib/lxd>//&:lxd-bilbeis_<var-lib-lxd>:unconfined (4402) 
   lxd-bilbeis_</var/lib/lxd>//&:lxd-bilbeis_<var-lib-lxd>:unconfined (4409) 
   lxd-bilbeis_</var/lib/lxd>//&:lxd-bilbeis_<var-lib-lxd>:unconfined (5379) 
   lxd-bilbeis_</var/lib/lxd>//&:lxd-bilbeis_<var-lib-lxd>:unconfined (5430) 
   lxd-bilbeis_</var/lib/lxd>//&:lxd-bilbeis_<var-lib-lxd>:unconfined (5944) 
   lxd-bilbeis_</var/lib/lxd>//&:lxd-bilbeis_<var-lib-lxd>:unconfined (5983) 
   lxd-bilbeis_</var/lib/lxd>//&:lxd-bilbeis_<var-lib-lxd>:unconfined (5989) 
   lxd-bilbeis_</var/lib/lxd>//&:lxd-bilbeis_<var-lib-lxd>:unconfined (6061) 
   lxd-bilbeis_</var/lib/lxd>//&:lxd-bilbeis_<var-lib-lxd>:unconfined (6130) 
   lxd-bilbeis_</var/lib/lxd>//&:lxd-bilbeis_<var-lib-lxd>:unconfined (6168) 
   lxd-bilbeis_</var/lib/lxd>//&:lxd-bilbeis_<var-lib-lxd>:unconfined (6211) 
   lxd-bilbeis_</var/lib/lxd>//&:lxd-bilbeis_<var-lib-lxd>:unconfined (6314) 
   lxd-bilbeis_</var/lib/lxd>//&:lxd-bilbeis_<var-lib-lxd>:unconfined (6416) 
   lxd-bilbeis_</var/lib/lxd>//&:lxd-bilbeis_<var-lib-lxd>:unconfined (6524) 
   lxd-bilbeis_</var/lib/lxd>//&:lxd-bilbeis_<var-lib-lxd>:unconfined (6680) 
   lxd-bilbeis_</var/lib/lxd>//&:lxd-bilbeis_<var-lib-lxd>:unconfined (6688) 
   lxd-bilbeis_</var/lib/lxd>//&:lxd-bilbeis_<var-lib-lxd>:unconfined (6719) 
   lxd-bilbeis_</var/lib/lxd>//&:lxd-bilbeis_<var-lib-lxd>:unconfined (6757) 
   lxd-bilbeis_</var/lib/lxd>//&:lxd-bilbeis_<var-lib-lxd>:unconfined (6791) 
   lxd-bilbeis_</var/lib/lxd>//&:lxd-bilbeis_<var-lib-lxd>:unconfined (6924) 
   lxd-bilbeis_</var/lib/lxd>//&:lxd-bilbeis_<var-lib-lxd>:unconfined (7009) 
   lxd-bilbeis_</var/lib/lxd>//&:lxd-bilbeis_<var-lib-lxd>:unconfined (7273) 
   lxd-bilbeis_</var/lib/lxd>//&:lxd-bilbeis_<var-lib-lxd>:unconfined (7274) 
   lxd-bilbeis_</var/lib/lxd>//&:lxd-bilbeis_<var-lib-lxd>:unconfined (7275) 
   lxd-bilbeis_</var/lib/lxd>//&:lxd-bilbeis_<var-lib-lxd>:unconfined (7276) 
   lxd-bilbeis_</var/lib/lxd>//&:lxd-bilbeis_<var-lib-lxd>:unconfined (7725) 
   lxd-hunan_</var/lib/lxd>//&:lxd-hunan_<var-lib-lxd>:unconfined (4172) 
   lxd-hunan_</var/lib/lxd>//&:lxd-hunan_<var-lib-lxd>:unconfined (4620) 
   lxd-hunan_</var/lib/lxd>//&:lxd-hunan_<var-lib-lxd>:unconfined (4676) 
   lxd-hunan_</var/lib/lxd>//&:lxd-hunan_<var-lib-lxd>:unconfined (5086) 
   lxd-hunan_</var/lib/lxd>//&:lxd-hunan_<var-lib-lxd>:unconfined (6175) 
   lxd-hunan_</var/lib/lxd>//&:lxd-hunan_<var-lib-lxd>:unconfined (6289) 
   lxd-hunan_</var/lib/lxd>//&:lxd-hunan_<var-lib-lxd>:unconfined (6806) 
   lxd-hunan_</var/lib/lxd>//&:lxd-hunan_<var-lib-lxd>:unconfined (6814) 
   lxd-hunan_</var/lib/lxd>//&:lxd-hunan_<var-lib-lxd>:unconfined (6864) 
   lxd-hunan_</var/lib/lxd>//&:lxd-hunan_<var-lib-lxd>:unconfined (6869) 
   lxd-hunan_</var/lib/lxd>//&:lxd-hunan_<var-lib-lxd>:unconfined (7022) 
   lxd-hunan_</var/lib/lxd>//&:lxd-hunan_<var-lib-lxd>:unconfined (7024) 
   lxd-hunan_</var/lib/lxd>//&:lxd-hunan_<var-lib-lxd>:unconfined (7033) 
   lxd-hunan_</var/lib/lxd>//&:lxd-hunan_<var-lib-lxd>:unconfined (7099) 
   lxd-hunan_</var/lib/lxd>//&:lxd-hunan_<var-lib-lxd>:unconfined (7100) 
   lxd-hunan_</var/lib/lxd>//&:lxd-hunan_<var-lib-lxd>:unconfined (7149) 
   lxd-hunan_</var/lib/lxd>//&:lxd-hunan_<var-lib-lxd>:unconfined (7269) 
   lxd-shanghai_</var/lib/lxd>//&:lxd-shanghai_<var-lib-lxd>:unconfined (26897) 
   lxd-shanghai_</var/lib/lxd>//&:lxd-shanghai_<var-lib-lxd>:unconfined (27001) 
   lxd-shanghai_</var/lib/lxd>//&:lxd-shanghai_<var-lib-lxd>:unconfined (27075) 
   lxd-shanghai_</var/lib/lxd>//&:lxd-shanghai_<var-lib-lxd>:unconfined (27171) 
   lxd-shanghai_</var/lib/lxd>//&:lxd-shanghai_<var-lib-lxd>:unconfined (27172) 
   lxd-shanghai_</var/lib/lxd>//&:lxd-shanghai_<var-lib-lxd>:unconfined (27173) 
   lxd-shanghai_</var/lib/lxd>//&:lxd-shanghai_<var-lib-lxd>:unconfined (27175) 
   lxd-shanghai_</var/lib/lxd>//&:lxd-shanghai_<var-lib-lxd>:unconfined (27178) 
   lxd-shanghai_</var/lib/lxd>//&:lxd-shanghai_<var-lib-lxd>:unconfined (27198) 
   lxd-shanghai_</var/lib/lxd>//&:lxd-shanghai_<var-lib-lxd>:unconfined (27297) 
   lxd-shanghai_</var/lib/lxd>//&:lxd-shanghai_<var-lib-lxd>:unconfined (27317) 
   lxd-shanghai_</var/lib/lxd>//&:lxd-shanghai_<var-lib-lxd>:unconfined (27388) 
   lxd-shanghai_</var/lib/lxd>//&:lxd-shanghai_<var-lib-lxd>:unconfined (27393) 
   lxd-shanghai_</var/lib/lxd>//&:lxd-shanghai_<var-lib-lxd>:unconfined (27397) 
   lxd-shanghai_</var/lib/lxd>//&:lxd-shanghai_<var-lib-lxd>:unconfined (27426) 
   lxd-shanghai_</var/lib/lxd>//&:lxd-shanghai_<var-lib-lxd>:unconfined (27429) 
   lxd-shanghai_</var/lib/lxd>//&:lxd-shanghai_<var-lib-lxd>:unconfined (27443) 
   lxd-shanghai_</var/lib/lxd>//&:lxd-shanghai_<var-lib-lxd>:unconfined (27449) 
   lxd-shanghai_</var/lib/lxd>//&:lxd-shanghai_<var-lib-lxd>:unconfined (27459) 
   lxd-shanghai_</var/lib/lxd>//&:lxd-shanghai_<var-lib-lxd>:unconfined (27469) 
   lxd-shanghai_</var/lib/lxd>//&:lxd-shanghai_<var-lib-lxd>:unconfined (27485) 
   lxd-shanghai_</var/lib/lxd>//&:lxd-shanghai_<var-lib-lxd>:unconfined (27487) 
   lxd-shanghai_</var/lib/lxd>//&:lxd-shanghai_<var-lib-lxd>:unconfined (27578) 
   lxd-shanghai_</var/lib/lxd>//&:lxd-shanghai_<var-lib-lxd>:unconfined (27582) 
   lxd-shanghai_</var/lib/lxd>//&:lxd-shanghai_<var-lib-lxd>:unconfined (27583) 
   lxd-shanghai_</var/lib/lxd>//&:lxd-shanghai_<var-lib-lxd>:unconfined (27584) 
   lxd-shanghai_</var/lib/lxd>//&:lxd-shanghai_<var-lib-lxd>:unconfined (27585) 
   lxd-shanghai_</var/lib/lxd>//&:lxd-shanghai_<var-lib-lxd>:unconfined (27604) 
   lxd-shanghai_</var/lib/lxd>//&:lxd-shanghai_<var-lib-lxd>:unconfined (27613) 
   lxd-shanghai_</var/lib/lxd>//&:lxd-shanghai_<var-lib-lxd>:unconfined (27615) 
   lxd-shanghai_</var/lib/lxd>//&:lxd-shanghai_<var-lib-lxd>:unconfined (27628) 
   lxd-shanghai_</var/lib/lxd>//&:lxd-shanghai_<var-lib-lxd>:unconfined (27629) 
   lxd-shanghai_</var/lib/lxd>//&:lxd-shanghai_<var-lib-lxd>:unconfined (27847) 
   lxd-shanghai_</var/lib/lxd>//&:lxd-shanghai_<var-lib-lxd>:unconfined (28223) 
   lxd-shanghai_</var/lib/lxd>//&:lxd-shanghai_<var-lib-lxd>:unconfined (28249) 
   lxd-shanghai_</var/lib/lxd>//&:lxd-shanghai_<var-lib-lxd>:unconfined (28258) 
   lxd-shanghai_</var/lib/lxd>//&:lxd-shanghai_<var-lib-lxd>:unconfined (28350) 
   lxd-shanghai_</var/lib/lxd>//&:lxd-shanghai_<var-lib-lxd>:unconfined (28354) 
   lxd-xian_</var/lib/lxd>//&:lxd-xian_<var-lib-lxd>:unconfined (5237) 
   lxd-xian_</var/lib/lxd>//&:lxd-xian_<var-lib-lxd>:unconfined (5518) 
   lxd-xian_</var/lib/lxd>//&:lxd-xian_<var-lib-lxd>:unconfined (5609) 
   lxd-xian_</var/lib/lxd>//&:lxd-xian_<var-lib-lxd>:unconfined (5908) 
   lxd-xian_</var/lib/lxd>//&:lxd-xian_<var-lib-lxd>:unconfined (7327) 
   lxd-xian_</var/lib/lxd>//&:lxd-xian_<var-lib-lxd>:unconfined (7441) 
   lxd-xian_</var/lib/lxd>//&:lxd-xian_<var-lib-lxd>:unconfined (7773) 
   lxd-xian_</var/lib/lxd>//&:lxd-xian_<var-lib-lxd>:unconfined (7784) 
   lxd-xian_</var/lib/lxd>//&:lxd-xian_<var-lib-lxd>:unconfined (7829) 
   lxd-xian_</var/lib/lxd>//&:lxd-xian_<var-lib-lxd>:unconfined (7866) 
   lxd-xian_</var/lib/lxd>//&:lxd-xian_<var-lib-lxd>:unconfined (7893) 
   lxd-xian_</var/lib/lxd>//&:lxd-xian_<var-lib-lxd>:unconfined (7896) 
   lxd-xian_</var/lib/lxd>//&:lxd-xian_<var-lib-lxd>:unconfined (7908) 
   lxd-xian_</var/lib/lxd>//&:lxd-xian_<var-lib-lxd>:unconfined (7947) 
   lxd-xian_</var/lib/lxd>//&:lxd-xian_<var-lib-lxd>:unconfined (7977) 
   lxd-xian_</var/lib/lxd>//&:lxd-xian_<var-lib-lxd>:unconfined (8019) 
   lxd-xian_</var/lib/lxd>//&:lxd-xian_<var-lib-lxd>:unconfined (8050) 
0 processes are in complain mode.
0 processes are unconfined but have a profile defined.
```

And on the container:
```
taha@shanghai:~
$ sudo aa-status 
apparmor module is loaded.
31 profiles are loaded.
31 profiles are in enforce mode.
   /snap/snapd/12159/usr/lib/snapd/snap-confine
   /snap/snapd/12159/usr/lib/snapd/snap-confine//mount-namespace-capture-helper
   /snap/snapd/12398/usr/lib/snapd/snap-confine
   /snap/snapd/12398/usr/lib/snapd/snap-confine//mount-namespace-capture-helper
   /usr/bin/man
   /usr/lib/NetworkManager/nm-dhcp-client.action
   /usr/lib/NetworkManager/nm-dhcp-helper
   /usr/lib/connman/scripts/dhclient-script
   /usr/lib/snapd/snap-confine
   /usr/lib/snapd/snap-confine//mount-namespace-capture-helper
   /usr/sbin/ntpd
   /usr/sbin/tcpdump
   /{,usr/}sbin/dhclient
   lsb_release
   man_filter
   man_groff
   nvidia_modprobe
   nvidia_modprobe//kmod
   snap-update-ns.lxd
   snap.lxd.activate
   snap.lxd.benchmark
   snap.lxd.buginfo
   snap.lxd.check-kernel
   snap.lxd.daemon
   snap.lxd.hook.configure
   snap.lxd.hook.install
   snap.lxd.hook.remove
   snap.lxd.lxc
   snap.lxd.lxc-to-lxd
   snap.lxd.lxd
   snap.lxd.migrate
0 profiles are in complain mode.
0 processes have profiles defined.
0 processes are in enforce mode.
0 processes are in complain mode.
0 processes are unconfined but have a profile defined.
```

What happens if we stop apparmor on the container?
```
$ sudo systemctl stop apparmor.service
```
Nothing, apparently. Even the output of `aa-status` did not change.

+ https://gitlab.com/apparmor/apparmor/-/wikis/AppArmor_Failures


## Refs

+ https://ubuntu.com/tutorials/introduction-to-lxd-projects
+ https://linuxcontainers.org/lxd/docs/master/
