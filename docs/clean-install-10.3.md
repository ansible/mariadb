# Installation of MariaDB 10.3 on fresh Ubuntu 20.04

This is how an installation of MariaDB 10.3 (from Ubuntu's own repos)
looked on an otherwise completely fresh Ubuntu 20.04 LXC container.

```
$ tree -a /etc/mysql/
/etc/mysql/
├── conf.d
│   ├── mysql.cnf
│   └── mysqldump.cnf
├── debian-start
├── debian.cnf
├── mariadb.cnf
├── mariadb.conf.d
│   ├── 50-client.cnf
│   ├── 50-mysql-clients.cnf
│   ├── 50-mysqld_safe.cnf
│   └── 50-server.cnf
├── my.cnf -> /etc/alternatives/my.cnf -> /etc/mysql/mariadb.cnf
└── my.cnf.fallback
```

> The MariaDB/MySQL tools read configuration files in the following order:
> 1. `/etc/mysql/mariadb.cnf` (this file) to set global defaults,
> 2. `/etc/mysql/conf.d/*.cnf` to set global options.
> 3. `/etc/mysql/mariadb.conf.d/*.cnf` to set MariaDB-only options.
> 4. `~/.my.cnf` to set user-specific options.
> (comment excerpt from the `/etc/mysql/mariadb.cnf` file)

Note: I have removed comments and empty lines from the `cat` output shown here.

```
$ cat /etc/mysql/mariadb.cnf 
[client-server]
!includedir /etc/mysql/conf.d/
!includedir /etc/mysql/mariadb.conf.d/
```

```
$ ll /etc/mysql/conf.d/
-rw-r--r-- 1 root root    8 Aug  3  2016 mysql.cnf
-rw-r--r-- 1 root root   55 Aug  3  2016 mysqldump.cnf
$ cat /etc/mysql/conf.d/mysql.cnf
[mysql]
$ cat /etc/mysql/conf.d/mysqldump.cnf 
[mysqldump]
quick
quote-names
max_allowed_packet	= 16M
```

```
$ ll /etc/mysql/mariadb.conf.d/
total 24
drwxr-xr-x 2 root root 4096 Jul 10 21:38 ./
drwxr-xr-x 4 root root 4096 Jul 10 21:39 ../
-rw-r--r-- 1 root root  733 May  9 18:20 50-client.cnf
-rw-r--r-- 1 root root  336 May  9 18:20 50-mysql-clients.cnf
-rw-r--r-- 1 root root 1032 May  9 18:20 50-mysqld_safe.cnf
-rw-r--r-- 1 root root 3940 May  9 18:20 50-server.cnf
$ cat /etc/mysql/mariadb.conf.d/50-client.cnf 
[client]
default-character-set = utf8mb4
socket = /var/run/mysqld/mysqld.sock
[client-mariadb]
$ cat /etc/mysql/mariadb.conf.d/50-mysql-clients.cnf 
[mysql]
default-character-set = utf8mb4
[mysql_upgrade]
[mysqladmin]
[mysqlbinlog]
[mysqlcheck]
[mysqldump]
[mysqlimport]
[mysqlshow]
[mysqlslap]
$ cat /etc/mysql/mariadb.conf.d/50-mysqld_safe.cnf 
# NOTE: This file is read only by the traditional SysV init script, not systemd.
# MariaDB systemd does _not_ utilize mysqld_safe nor read this file.
# For similar behaviour, systemd users should create the following file:
# /etc/systemd/system/mariadb.service.d/migrated-from-my.cnf-settings.conf
# For more information, please read https://mariadb.com/kb/en/mariadb/systemd/
[mysqld_safe]
socket		= /var/run/mysqld/mysqld.sock
nice		= 0
skip_log_error
syslog
$ cat /etc/mysql/mariadb.conf.d/50-server.cnf
[server]
[mysqld]
user                  = mysql
pid-file              = /run/mysqld/mysqld.pid
socket                = /run/mysqld/mysqld.sock
basedir               = /usr
datadir               = /var/lib/mysql
tmpdir                = /tmp
lc-messages-dir       = /usr/share/mysql
bind-address          = 127.0.0.1
query_cache_size      = 16M
log_error             = /var/log/mysql/error.log
expire_logs_days      = 10
character-set-server  = utf8mb4
collation-server      = utf8mb4_general_ci
[embedded]
[mariadb]
[mariadb-10.3]
```

Note that `mysqld.sock` and `mysqld.pid` files are found in two locations (for some reason):

```
$ ll /run/mysqld/
-rw-rw----  1 mysql mysql   5 Jul 10 21:39 mysqld.pid
srwxrwxrwx  1 mysql mysql   0 Jul 10 21:39 mysqld.sock=

$ ll /var/run/mysqld/
-rw-rw----  1 mysql mysql   5 Jul 10 21:39 mysqld.pid
srwxrwxrwx  1 mysql mysql   0 Jul 10 21:39 mysqld.sock=
```

Points to `/var/run/mysqld/mysqld.sock`:
```
/etc/mysql/mariadb.conf.d/50-client.cnf
/etc/mysql/mariadb.conf.d/50-mysqld_safe.cnf
```

Points to `/run/mysqld/`:
```
/etc/mysql/mariadb.conf.d/50-server.cnf
```

I don't know why the MariaDB install does this.

The MariaDB service appears to have started without issues, though.
Logging in to mysql with `sudo mysql` worked as well.
