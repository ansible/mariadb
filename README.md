# MariaDB

Installs and configures MariaDB server on Ubuntu 18.04 or 20.04.

From now on, this role will be geared specifically towards MariaDB.
If it also happens to work for MySQL, great, but if there is some
incompatibility, this role will support the MariaDB way.

When it comes to MariaDB/MySQL, I do have a number of servers 
that were provisioned with an earlier version of this role,
but in contrast to PHP I have probably never customised the MySQL
settings.

Therefore I think I can consider myself in the fortunate position 
of having no technical debt in regards to this role. And since
I have no custom MySQL settings I need to maintain 
backwards-compatibility for, why not make a fresh start in this role?

My own limited tests on Ubuntu 20.04 indicate that MariaDB v10.4
has a better organised configuration than v10.3.
Installing 10.4 requires us to install from the MariaDB repo,
but that is a small price to pay.

This role is therefore geared towards MariaDB v10.4 and above.


## Dependencies

None.

## Example playbook

This role has no special requirements.
Note that this role requires root access, so either run it in a playbook with a 
global `become: yes`, or invoke the role in your playbook like:

```
- hosts: db-servers
  roles:
    - { role: mariadb, become: true }
```


## A note on authentication in MariaDB 10.4 and higher

> The `root@localhost` user account created by `mysql_install_db` is created 
> with the ability to use two authentication plugins.
> https://mariadb.com/kb/en/authentication-from-mariadb-104

> First, it is configured to try to use the `unix_socket` authentication plugin. 
> This allows the `root@localhost` user to login without a password via the local 
> Unix socket file defined by the `socket` system variable, as long as the login 
> is attempted from a process owned by the operating system `root` user account.

> Second, if authentication fails with the `unix_socket` authentication plugin, 
> then it is configured to try to use the `mysql_native_password` authentication plugin. 
> However, an invalid password is initially set, so in order to authenticate this way, 
> a password must be set with `SET PASSWORD`.

> Two all-powerful accounts are created by default - `root` and the OS user that 
> owns the data directory, typically `mysql`.

+ https://mariadb.com/kb/en/authentication-from-mariadb-104/
+ https://mariadb.org/authentication-in-mariadb-10-4/
+ https://reddit.com/r/sysadmin/comments/avx1u6/how_to_change_the_root_password_with_mariadb_104/


## Refs

This role was derived from 
[geerlingguy/ansible-role-mysql](https://github.com/geerlingguy/ansible-role-mysql).

And inspired by other Ansible roles:

+ https://github.com/fauust/ansible-role-mariadb (last commit 2021-06-29, Ubuntu)
+ https://github.com/mahdi22/ansible-mariadb-install (last commit 2020-10-02, Ubuntu <=20.04)
+ https://github.com/tschifftner/ansible-role-mariadb (last commit 2018-09-06, Ubuntu <=18.04)
+ https://github.com/bertvv/ansible-role-mariadb (last commit 2020-10-15, CentOS 7)
+ https://github.com/PCextreme/ansible-role-mariadb (last commit 2018-01-18, Ubuntu <=16.04)
+ https://github.com/nover/ansible-role-mariadb (last commit 2016-05-07, Ubuntu 14.04)


Other refs:

+ https://dba.stackexchange.com/questions/274547/cant-set-root-password-after-fresh-installation-of-mariadb-using-ansible
+ https://stackoverflow.com/questions/43415961/ansible-setup-mysql-root-password
+ https://downloads.mariadb.org/mariadb/repositories/#distro_release=bionic--ubuntu_bionic&mirror=agdsn&version=10.4
+ https://serversforhackers.com/c/mysqldump-with-modern-mysql
